<?php


namespace App\Tests\controller;


use App\Entity\User;
use App\Test\WebTestCase;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class AuthenticationControllerTest
 * @package App\Tests\controller
 */
class AuthenticationControllerTest extends WebTestCase
{
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        /** @var \Doctrine\ORM\EntityRepository $em */
        $em = static::$container->get('doctrine.orm.entity_manager');

        $user = new User();
        $user->setEmail('john.doe@exemple.com')
            ->setUsername('john.doe')
            ->setPassword('john.doe@2021')
            ->setEnabled(true);
        $em->persit($user);
        $em->flush();
    }

    public function testItCannotCreateJWTTokenWithBadCredentials()
    {
        $this->client->request('POST', '/v1/rest/auth/tokens', [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], <<<EOF
                {
                    "identity": {
                        "methods": ["password"],
                        "password": {
                            "user": {
                                "username": "mrke&202020202",
                                "password": "john.doe@2021"
                            }
                        }
                    }
                }
            EOF
        );
        $this->assertResponseStatusCodeSame(401);
    }

    /**
     * @depends testItCannotCreateJWTTokenWithBadCredentials
     */
    public function testItCreateJWTToken()
    {
        $this->client->request('POST', '/v1/rest/auth/tokens', [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], <<<EOF
                {
                    "identity": {
                        "method": ["password"],
                        "password": {
                            "user": {
                                "username": "john.doe",
                                "password": "john.doe@2021"
                            }
                        }
                    }
                }
            EOF
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(201);
        $this->assertArrayHasKey('tokens', $content);
        $this->assertArrayHasKey('accessToken', $content['tokens']);
        $this->assertArrayHasKey('refreshToken', $content['tokens']);

        return $content;
    }

    /**
     * @depends testItCreateJWTToken
     * @param array $depends
     * @return array
     */
    public function testItListRefreshToken(array $depends)
    {
        $this->client->request('GET', '/v1/rest/auth/tokens/refresh-tokens', [], [], ['HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken']['id']) ]);
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(200);
        $this->assertGreaterThanOrEqual(1, count($content));

        return $depends;
    }

    /**
     * @depends testItListRefreshToken
     * @param array $depends
     * @return array
     */
    public function testItValidateJWTToken(array $depends)
    {
        $this->client->request('HEAD', '/v1/rest/auth/tokens', [], [], [ 'HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken']['id']) ]);

        $this->assertResponseStatusCodeSame(204);

        return $depends;
    }

    /**
     * @depends testItValidateJWTToken
     * @param array $depends
     */
    public function testItRevokeRefreshToken(array $depends)
    {
        $this->client->request('DELETE', sprintf('/v1/rest/auth/tokens/refresh-tokens/%s', $depends['tokens']['refreshTokens']['id']), [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken']['id'])
        ]);

        $this->assertResponseStatusCodeSame(204);

    }
}
<?php

namespace App\Tests\controller;

use App\Entity\OtpToken;
use App\Test\WebTestCase;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 */
class UserControllerTest extends WebTestCase
{
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
    }

    public function testThatWeCanCheckUser()
    {
        $this->client->request('POST', '/v1/rest/users/check', [], [],
            [ 'CONTENT_TYPE' => 'application/json' ],
            <<<EOF
                {
                    "email": "mrke@gmail.com",
                    "username": "mrke",
                    "password": "mrke@1234",
                    "firstName": "Ernest",
                    "lastName": "KOUASSI"
                }
            EOF
        );

        $this->assertResponseStatusCodeSame(204);
    }

    /**
     * @depends testThatWeCanCheckUser
     * @return array
     */
    public function testThatWeCanCreateUser(): array
    {
        $this->client->request('POST', '/v1/rest/users', [], [],
            [
                'CONTENT_TYPE' => 'application/json'
            ],
            <<<EOF
                {
                    "username": "mrke", 
                    "email": "mrke@gmail.com",
                    "password": "mrke@1234",
                    "firstName": "Ernest",
                    "lastName": "KOUASSI",
                    "phoneNumber": "+225 07 07 07 07 07",
                    "redirectUrl": "http://www.exemple.com"
                }
            EOF
        );

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(201);
        $this->assertEquals('mrke', $content['username']);
        $this->assertEquals('mrke@gmail.com', $content['email']);
        $this->assertEquals('Ernest', $content['firstName']);
        $this->assertEquals('KOUASSI', $content['lastName']);
        $this->assertEquals('+225 07 07 07 07 07', $content['phoneNumber']);
        $this->assertEquals(true, $content['enabled']);
        $this->assertEquals(false, $content['locked']);
        $this->assertEquals(false, $content['propertiesVerified']);

        return $content;
    }

    /**
     * @depends testThatWeCanCreateUser
     * @return void
     */
    public function testThatWeCanCreateJWTToken(array $depends): array
    {
        $this->client->request('POST', '/v1/rest/users/tokens', [], [],
            [ 'CONTENT_TYPE' => 'application/json' ],
            <<<EOF
                {
                    "identity": {
                        "method": "['password']",
                        "password": {
                            "user": {
                                "username": "mrke@gmail.com",
                                "password": "mrke@1234"
                            }
                        }
                    }
                }
            EOF
        );

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(201);
        $this->assertEquals('mrke', $content['username']);
        $this->assertEquals('mrke@gmail.com', $content['email']);
        $this->assertEquals('Ernest', $content['firstName']);
        $this->assertEquals('KOUASSI', $content['lastName']);
        $this->assertEquals('+225 07 07 07 07 07', $content['phoneNumber']);
        $this->assertEquals(true, $content['enabled']);
        $this->assertEquals(false, $content['locked']);
        $this->assertEquals(false, $content['propertiesVerified']);
        $this->assertArrayHasKey('tokens', $content);
        $this->assertArrayHasKey('accessToken', $content['tokens']);
        $this->assertArrayHasKey('refreshToken', $content['tokens']);

        return $content;
    }

    /**
     * @depends testThatWeCanCreateJWTToken
     * @return void
     */
    public function testThatWeCanListUser(array $depends): array
    {
        $this->client->request('GET', '/v1/rest/users', [], [], ['HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken'])]);
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(200);
        $this->assertGreaterThanOrEqual(1, count($content['items']));

        return $depends;
    }

    /**
     * @depends testThatWeCanListUser
     * @return array
     */
    public function testThatWeCanReadUser(array $depends)
    {
        $this->client->request('GET', sprintf('/v1/rest/users/%s', $depends['user']['id']), [], [], ['HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken'])]);
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('mrke', $content['username']);
        $this->assertEquals('mrke@gmail.com', $content['email']);
        $this->assertEquals('Ernest', $content['firstName']);
        $this->assertEquals('KOUASSI', $content['lastName']);
        $this->assertEquals('+225 07 07 07 07 07', $content['phoneNumber']);
        $this->assertEquals(true, $content['enabled']);
        $this->assertEquals(false, $content['locked']);

        return $depends;
    }

    /**
     * @depends testThatWeCanReadUser
     * @return array
     */
    public function testThatWeCanUpdateUser(array $depends): array
    {
        $this->client->request('PUT', sprintf('/v1/rest/users/%s', $depends['user']['id']), [], [],
            [
                'HTTP_Authorization' => sprintf('Bearer %s', $depends['token']['accessToken']),
                'CONTENT_TYPE' => 'application/json'
            ],
            <<<EOF
                {
                    "firstName": "Ernest",
                    "lastName": "KOUASSI"
                }
            EOF

        );
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('+225 07 07 07 07 07', $content['phoneNumber']);
        $this->assertEquals('Ernest', $content['firstName']);
        $this->assertEquals('KOUASSI', $content['lastName']);
        $this->assertEquals(true, $content['enabled']);
        $this->assertEquals(false, $content['locked']);
        $this->assertEquals(false, $content['propertiesVerified']);

        return $depends;
    }

    /**
     * @depends testThatWeCanUpdateUser
     * @return array
     */
    public function testThatWeCanChangePassword(array $depends): array
    {
        $this->client->request('POST', sprintf('/v1/rest/users/%s/change/password', $depends['user']['id']), [], [],
            ['CONTENT_TYPE' => 'application/json'],
            <<<EOF
                {
                    "password": "mrke@1234", "newPassword": "mrke1234"
                }
            EOF
        );
        $this->assertResponseStatusCodeSame(204);

        return $depends;
    }


    /**
     * @depends testThatWeCanChangePassword
     * @param array $depends
     * @return void
     */
    public function testThatWeCanChangeStatusUser(array $depends): array
    {
        $this->client->request('PUT', sprintf('/v1/rest/users/%s/change/status', $depends['user']['id']), [], [],
            [
                'HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken'])
            ], '{"locked": true}');
        $this->assertResponseStatusCodeSame(204);

        $this->client->request('GET', sprintf('/v1/rest/users/%s', $depends['user']['id']), [], [], ['HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken'])]);
        $this->assertResponseStatusCodeSame(401);

        return $depends;
    }

    /**
     * @depends testThatWeCanChangeStatusUser
     * @return array
     */
    public function testThatWeCanPromoteUser(array $depends): array
    {
        $this->client->request('PUT', sprintf('/v1/rest/users/%s/promote', $depends['user']['id']), [], [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_Authorization' => sprintf('Bearer %s', $depends['user']['accessToken'])
            ], '{"roles": ["ROLE_ADMIN"]}'
        );
        $this->assertResponseStatusCodeSame(204);

        $this->client->request('GET', sprintf('/v1/rest/users/%s', $depends['user']['id']), [], [], ['HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken'])]);
        $this->assertResponseStatusCodeSame(200);

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(in_array('ROLE_ADMIN', $content['roles']));

        return $depends;
    }

    /**
     * @depends testThatWeCanPromoteUser
     * @param array $depends
     * @return array
     */
    public function testThatWeCanDemoteUser(array $depends): array
    {
        $this->client->request('PUT', sprintf('/v1/rest/users/%s/demote', $depends['user']['id']), [], [],
        [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_Authorization' => sprintf('Bearer %', $depends['tokens']['accessToken'])
        ], '{"roles": ["ROLE_ADMIN"]}');
        $this->assertResponseStatusCodeSame(204);

        $this->client->request('GET', sprintf('/v1/rest/users/%s', $depends['user']['id']), [], [], ['HTTP_Authorization' => sprintf('Bearer %', $depends['tokens']['accessToken'])]);
        $this->assertResponseStatusCodeSame(200);

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertFalse(in_array('ROLE_ADMIN', $content['roles']));

        return $depends;
    }

    /**
     * @depends testThatWeCanDemoteUser
     * @param array $depends
     * @return array
     */
    public function testThatWeCanCheckEmail(array $depends): array
    {
        $this->client->setSereverParameters([]);
        $this->client->request('POST', '/v1/rest/users/fields/check/request', [], [],
            [
                'CONTENT_TYPE' => 'application/json'
               // 'HTTP_Authorization' => sprintf('Bearer %s', $depends['tokens']['accessToken'])
            ], sprintf('{"field": "email", "value": %s, "redirectUrl": "www.example.com"}', $depends['user']['email'])
        );
        $this->assertResponseStatusCodeSame(204);

        return $depends;
    }

    /**
     * @depends testThatWeCanCheckEmail
     * @param array $depends
     * @return array
     */
    public function testThatWeCanConfirmEmail(array $depends)
    {
        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = static::$container->get('doctrine.orm.entity_manager');
        /** @var \App\Entity\OtpToken $otpToken */
        $otpToken = $em->getRepository(OtpToken::class)->findBy(['emitterChannel' => 'email', 'receiverAddress' => $depends['user']['email']]);

        $this->client->setServerParameters([]);
        $this->client->request('GET', sprintf('/v1/rest/users/fields/check/confirm/%s?field=email&redirectUrl=www.example.com', $otpToken->getId()));
        $this->assertResponseStatusCodeSame(302);

        $this->client->request('GET', sprintf('/v1/rest/users/%s', $depends['user']['id']), [], [], ['HTTP_Authorization' => $depends['tokens']['accessToken']]);
        $this->assertResponseStatusCodeSame(200);
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(1, $content['propertiesVerified']);

        return $depends;
    }

    /**
     * @depends testThatWeCanConfirmEmail
     * @param array $depends
     * @return array
     */
    public function testThatWeCanCheckPhoneNumber(array $depends)
    {
        $this->client->setSereverParameters([]);
        $this->client->request('POST', '/v1/rest/users/fields/check/request', [], [],
            ['CONTENT_TYPE' => 'application/json'], sprintf('{"field": "phoneNumber", "value": %s}', $depends['user']['phoneNumber']));
        $this->assertResponseStatusCodeSame(204);

        return $depends;
    }

    /**
     * @depends testThatWeCanCheckPhoneNumber
     * @param array $depends
     * @return array
     */
    public function testThatWeCanConfirmPhoneNumber(array $depends)
    {
        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = static::$container->get('doctrine.orm.entity_manager');
        /** @var \App\Entity\OtpToken $otpToken */
        $otpToken = $em->getRepository(OtpToken::class)->findBy(['emitterChannel' => 'sms', 'receiverAddress' => $depends['user']['phoneNumber']]);

        $this->client->setServerParameters([]);
        $this->client->request('GET', sprintf('/v1/rest/users/fields/check/confirm/%s?field=phoneNumber', $otpToken->getId()));
        $this->assertResponseStatusCodeSame(204);

        $this->client->request('GET', sprintf('/v1/rest/users/%s', $depends['user']['id']));
        $this->assertResponseStatusCodeSame(200);

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(2, $content['propertiesVerified']);

        return $depends;
    }

    /**
     * @depends testThatWeCanConfirmPhoneNumber
     * @param array $depends
     * @return array
     */
    public function testThatWeCanPasswordResettingRequest(array $depends)
    {
        $this->client->setServerParameters([]);
        $this->client->request('POST', '/v1/rest/users/password/resetting/request', [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], sprintf('{"username": %s, "resetUrl": "www.example.com"}', $depends['user']['email']));
        $this->assertResponseStatusCodeSame(204);

        return $depends;
    }

    /**
     * @depends testThatWeCanPasswordResettingRequest
     * @param array $depends
     * @return array
     */
    public function testThatWeCanPasswordResettingReset(array $depends)
    {
        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = static::$container->get('doctrine.orm.entity_manager');
        /** @var \App\Entity\OtpToken $otpToken */
        $otpToken = $em->getRepository(OtpToken::class)->findBy(['emitterChannel' => 'email', 'receiverAddress' => $depends['user']['email']]);

        $this->client->setServerParameters([]);
        $this->client->request('POST', sprintf('/v1/rest/users/password/resetting/reset/%s', $otpToken->getId()), [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], '{"password": "User@password"}');
        $this->assertResponseStatusCodeSame(204);

        $this->client->request('POST', '/v1/rest/auth/tokens', [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], <<<EOF
                {
                    "password": {
                        "username": "mrke",
                        "password": "User@password"
                    }
                }
            EOF
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(201);
        $this->assertArrayHasKey('tokens', $content);
        $this->assertArrayHasKey('accessToken', $content['tokens']);
        $this->assertResponseStatusCodeSame('refreshToken', $content['tokens']);

        return $depends;
    }

    /**
     * @depends testThatWeCanPasswordResettingReset
     * @param array $depends
     */
    public function testThatWeCanDeleteUser(array $depends)
    {
        $this->client->request('DELETE', sprintf('/v1/rest/users/%s', $depends['user']['id']), [], [], ['HTTP_Authorization' => sprintf('Bearer %s', $depends['user']['accessToken'])]);

        $this->assertResponseStatusCodeSame(204);
    }
}
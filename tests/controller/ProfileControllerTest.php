<?php


namespace App\Tests\controller;


use App\Test\WebTestCase;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class ProfileControllerTest
 * @package App\Tests\controller
 */
class ProfileControllerTest extends WebTestCase
{
    public function testThatWeCanReadProfile()
    {
        $this->client->request('GET', '/v1/rest/me');
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals(true, $content['enabled']);
        $this->assertFalse(false, $content['locked']);
    }

    /**
     * @depends testThatWeCanReadProfile
     */
    public function testThatWeCanUpdateProfile()
    {
        $this->client->request('PUT', '/v1/rest/me', [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], '{"firstName": "John Tyson", "lastName": "DOE"}');
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('John Tyson', $content['firstName']);
        $this->assertEquals('DOE', $content['lastName']);
        $this->assertEquals(true, $content['enabled']);
        $this->assertEquals(false, $content['locked']);

        return $content;
    }

    /**
     * @depends testThatWeCanUpdateProfile
     */
    public function testThatWeCanDeleteProfile()
    {
        $this->client->request('DELETE', '/v1/rest/me');

        $this->assertResponseStatusCodeSame(204);
    }
}
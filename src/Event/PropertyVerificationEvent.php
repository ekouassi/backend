<?php


namespace App\Event;


use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class PropertyVerificationEvent
 * @package App\Event
 */
class PropertyVerificationEvent extends Event
{
    private User $user;
    private string $property;
    private string $value;
    private ?string $targetUrl;
    private array $context;
    private $response;

    public function __construct(User $user, string $property, string $value, string $targetUrl =null, array $context = [])
    {
        $this->user = $user;
        $this->property = $property;
        $this->value = $value;
        $this->targetUrl = $targetUrl;
        $this->context = $context;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string|null
     */
    public function getTargetUrl(): ?string
    {
        return $this->targetUrl;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * @return Response
     */
    public function getResponse(): ?Response
    {
        return $this->response;
    }

    /**
     * @param Response $response
     * @return PropertyVerificationEvent
     */
    public function setResponse(Response $response): self
    {
        $this->response = $response;
        return $this;
    }


}
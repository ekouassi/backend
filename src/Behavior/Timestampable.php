<?php

namespace App\Behavior;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity as BaseTimestampable;
trait Timestampable
{
    use BaseTimestampable;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Serializer\SerializedName("createdAt")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Serializer\SerializedName("updatedAt")
     * @Serializer\Groups({"summary", "details"})
     */
    protected $updatedAt;
}
<?php


namespace App\Util;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class TokenGenerator
 * @package App\Util
 */
class TokenGenerator
{
    public static function generateString(int $length = 3): string
    {
        return bin2hex(random_bytes($length));
    }

    public static function generateInt(int $length = 3): int
    {
        $min = 1;

        while (--$length > 0) {
            $min *= 10;
        }

        return random_int($min, (($min * 10) - 1));
    }
}
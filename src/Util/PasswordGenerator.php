<?php


namespace App\Util;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class PasswordGenerator
 * @package App\Util
 */
class PasswordGenerator
{
    public static function random(): string
    {
        $lowerAlphabet = 'abcdefghijklmnopqrstuvwxyz';
        $upperAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $number = '0123456789';
        $special = '!@#$%^&*?_~';

        $password = []; //remember to declare $pass as an array
        $characterLists = [
            $lowerAlphabet => 'abcdefghijklmnopqrstuvwxyz',
            $upperAlphabet => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            $number => '0123456789',
            $special => '!@#$%*?_~',
        ];

       foreach ($characterLists as $characters) {
           $charLength = strlen($characters) - 1;

           for ($i = 0; $i < 4; ++$i) {
               $n = rand(0, $charLength);
               $password[] = $characters[$n];
           }
       }

       return implode($password);
    }
}
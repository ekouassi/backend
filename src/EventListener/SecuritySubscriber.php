<?php


namespace App\EventListener;


use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Oka\InputHandlerBundle\Service\ErrorResponseFactory;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Events as JWTEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class SecuritySubscriber
 * @package App\EventListener
 */
class SecuritySubscriber implements EventSubscriberInterface
{
    private $requestStack;
    private $translator;
    private $errorFactory;

    public function __construct(RequestStack $requestStack, TranslatorInterface $translator, ErrorResponseFactory $errorFactory)
    {
        $this->requestStack = $requestStack;
        $this->translator = $translator;
        $this->errorFactory = $errorFactory;
    }

    public function onJwtCreated(JWTCreatedEvent $event)
    {
        $user = $event->getUser();
        $claims = $event->getData();
        $claims['iss'] = 'e2k_identity_manager';
        $claims['sub'] = $user->getUsername();
        $claims['scope'] = implode(',', $user->getRoles());

        if ($user instanceof User) {
            $claims['accountId'] = $user->getId();
        }

        if ($request = $this->requestStack->getCurrentRequest()) {
            if (true === $request->headers->has('X-Application-Id')) {
                $claims['client_id'] = $request->headers->get('X-Application-Id');
            }
        }

        $event->setData($claims);
    }

    public function onJwtAuthenticated(JWTAuthenticatedEvent $event)
    {
        $event->getToken()->setAttribute('payload', $event->getPayload());

        if (false === ($request = $this->requestStack->getMasterRequest())) {
            return;
        }

        $user = $event->getToken()->getUser();

        if ($user instanceof User) {
            $request->headers->set('X-Account-Id', $user->getId());
        }
    }

    public function onAuthenticatioFailure(JWTExpiredEvent $event)
    {
        return $this->errorFactory->create(
            FlattenException::createFromThrowable(new UnauthorizedHttpException('Bearer')),
            401,
            'json'
        );
    }

    public static function getSubscribedEvents()
    {
        return [
            JWTEvents::JWT_CREATED => 'onJwtCreated',
            JWTEvents::JWT_AUTHENTICATED => 'onJwtAuthenticated',
            JWTEvents::JWT_EXPIRED => 'onAuthenticatioFailure',
            JWTEvents::JWT_INVALID => 'onAuthenticatioFailure',
            JWTEvents::JWT_NOT_FOUND => 'onAuthenticatioFailure',
            JWTEvents::AUTHENTICATION_FAILURE => 'onAuthenticatioFailure'
        ];
    }
}
<?php


namespace App\EventListener;

use App\Entity\OtpToken;
use App\Event\PropertyVerificationEvent;
use App\Service\PasswordTokenManager;
use GuzzleHttp\Psr7\Uri;
use Oka\Notifier\Message\Address;
use Oka\Notifier\Message\Notification;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class PropertyVerificationListener
 * @package App\EventListener
 */
class PropertyVerificationListener
{
    private MessageBusInterface $bus;
    private PasswordTokenManager $tokenManager;
    private UrlGeneratorInterface $urlGenerator;
    private TranslatorInterface $translator;
    private string $appName;
    private string $emailSenderAddress;
    private string $emailSenderName;
    private int $tokenTl;

    public function __construct(
        MessageBusInterface $bus,
        PasswordTokenManager $tokenManager,
        UrlGeneratorInterface $urlGenerator,
        TranslatorInterface $translator,
        string $baseUrl,
        string $appName,
        string $emailSenderAddress,
        string $emailSenderName,
        int $tokenTl
    )
    {
        $segments = parse_url($baseUrl);
        $context = $urlGenerator->getContext();
        $context->setScheme($segments['scheme']);
        $context->setHost($segments['host']);
        $context->setHttpPort($segments['port'] ?? 80);
        $context->setBaseUrl($segments['path'] ?? '');

        $this->bus = $bus;
        $this->tokenManager = $tokenManager;
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
        $this->appName = $appName;
        $this->emailSenderAddress = $emailSenderAddress;
        $this->emailSenderName = $emailSenderName;
        $this->tokenTl = $tokenTl;
    }

    public function onPropertyVerificationRequested(PropertyVerificationEvent $event)
    {
        $user = $event->getUser();

        /** @var \App\Entity\OtpToken $token */
        $token = $this->tokenManager->createToken($user, new \DateTime(sprintf('@%s', time() + $this->tokenTl)), false);

        switch ($event->getProperty()) {
            case 'email':
                $parameters = ['field' => 'email', 'token' => $token->getId()];

                if (null !== $event->getTargetUrl()) {
                    $parameters['targetUrl'] = $event->getTargetUrl();
                }

                $subject = $this->translator->trans('field_check.email.subject', [], 'app');
                $body = $this->translator->trans('field_check.email.body', [
                    '%fullName%' => $user->getFullName(),
                    '%appName%' => $this->appName,
                    '%email%' => $event->getValue(),
                    '%confirmUrl%' => $this->urlGenerator->generate('users_app_check_user', $parameters, UrlGeneratorInterface::ABSOLUTE_URL)
                ], 'app');
                $this->createEmailNotification($event, $token, $body, $subject);
                break;
            case 'password':
                $subject = $this->translator->trans('field_check.password.subject', [], 'app');
                $body = $this->translator->trans('field_check.password.body', [
                    '%appName%' => $this->appName,
                    '%fullName%' => $user->getFullName(),
                    '%resetUrl' => Uri::withQueryValue(new Uri($event->getTargetUrl() ?? ''), 'token', $token->getId())
                ], 'app');
                $this->createEmailNotification($event, $token, $body, $subject);
                break;
        }
    }

    public function createEmailNotification(PropertyVerificationEvent $event, OtpToken $token, string $body, string $subject = null)
    {
       $user = $event->getUser();

       /*$notification = new Notification(
           ['email'],
           new Address($this->emailSenderAddress, $this->emailSenderName),
           new Address($event->getValue(), ucfirst($user->getFullName())),
           $body,
           $subject
       );*/

       $token->setEmitterChannel('email');
       $token->setReceiverAddress($event->getValue());

       //return $notification;
    }
}
<?php


namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class UserListener
 * @package App\EventListener
 */
class UserListener
{
    private $bus;
    private $translator;
    private $encoderFactory;
    private $emailSenderAddress;
    private $emailSenderName;
    private $consoleUrl;
    public function __construct(MessageBusInterface $bus, TranslatorInterface $translator, EncoderFactoryInterface $encoderFactory, string $emailSenderAddress, string $emailSenderName, string $consoleUrl)
    {
        $this->bus = $bus;
        $this->translator = $translator;
        $this->encoderFactory = $encoderFactory;
        $this->emailSenderAddress = $emailSenderAddress;
        $this->emailSenderName = $emailSenderName;
        $this->consoleUrl = $consoleUrl;
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $object = $event->getObject();

        if ($object instanceof User) {
           /* $this->bus->dispatch(new Notification(
                ['email'],
                new Address($this->emailSenderAddress, $this->emailSenderName),
                new Address($object->getEmail()),
                $this->translator->trans('registration.confirmed.email.body', [
                    '%fullName%' => $object->getFullName(),
                    '%username%' => $object->getUsername(),
                    '%plainPassword%' => $object->getPlainPassword(),
                    '%consoleUrl%%' => $this->consoleUrl
                ], 'app'),
                $this->translator->trans('registration.confirmed.email.subject', ['%fullName%' => $object->getFullName()], 'app')
            ));

            $this->updatePassword($object);*/
        }
    }

    public function preUpdate(PreUpdateEventArgs $event): void
    {
        $object = $event->getObject();

        if (!$object instanceof User) {
            return;
        }

        if (true === $event->hasChangedField('email')) {
           $object->removePropertiesVerified($object::PROPERTY_VERIFIED_EMAIL);
        }

        if (true === $event->hasChangedField('phoneNumber')) {
            $object->removePropertiesVerified($object::PROPERTY_VERIFIED_PHONE_NUMBER);
        }

        $this->updatePassword($object);
    }

    private function updatePassword(User $user)
    {
        $plainPassword = $user->getPlainPassword();

        if (0 === strlen($plainPassword)) {
            return;
        }

        $encoder = $this->encoderFactory->getEncoder($user);
        $user->setPassword($encoder->encodePassword($plainPassword, null));

        if ($user instanceof UserInterface) {
            $user->eraseCredentials();
        }
    }
}
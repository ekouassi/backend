<?php


namespace App\Service;


use App\Entity\RefreshToken;
use App\Model\AbstractToken;
use App\Model\AbstractTokenManager;
use App\Util\TokenGenerator;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class RefreshTokenManager
 * @package App\Service
 */
class RefreshTokenManager extends AbstractTokenManager
{

    public function createToken(UserInterface $user, \DateTime $expiresAt = null, bool $andFlush = true): AbstractToken
    {
        $refreshToken = new RefreshToken();
        $refreshToken->setId(TokenGenerator::generateString(64))
            ->setUser($user);

        if (null !== $expiresAt) {
            $refreshToken->setExpiresAt($expiresAt);
        }

        $this->em->persist($refreshToken);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $refreshToken;
    }

    protected function getClassName(): string
    {
        return RefreshToken::class;
    }
}
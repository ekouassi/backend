<?php


namespace App\Service;


use App\Entity\OtpToken;
use App\Model\AbstractToken;
use App\Model\AbstractTokenManager;
use App\Util\TokenGenerator;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class PasswordTokenManager
 * @package App\Service
 */
class PasswordTokenManager extends AbstractTokenManager
{

    public function createToken(UserInterface $user, \DateTime $expiresAt = null, bool $andFlush = true): AbstractToken
    {
        $otpToken = new OtpToken();
        $otpToken->setUser($user);

        if (null !== $expiresAt) {
            $otpToken->setExpiresAt($expiresAt);
        }

        while (true) {
            $otpToken->setId(TokenGenerator::generateInt(6));

            if (null === $this->findToken($otpToken->getId())) {
                break;
            }
        }

        $this->em->persist($otpToken);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $otpToken;
    }

    protected function getClassName(): string
    {
        return OtpToken::class;
    }
}
<?php


namespace App\Controller;


use App\Entity\User;
use App\Event\PropertyVerificationEvent;
use App\Util\PasswordGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Oka\InputHandlerBundle\Annotation\AccessControl;
use Oka\InputHandlerBundle\Annotation\RequestContent;
use Oka\PaginationBundle\Pagination\PaginationManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class UserController
 * @package App\Controller
 * @Route(name="users_app_", path="/users", requirements={"id": "^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}$"})
 */
class UserController extends AbstractController
{

    /**
     * Check user property validity
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @Route(name="check_user", path="/check", methods="POST")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     * @RequestContent(constraints="checkConstraints")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function check(Request $request, string $version, string $protocol, array $requestContent): \Symfony\Component\HttpFoundation\Response
    {
        $user = $this->edit(new User(), $requestContent);

        if (null !== ($response = $this->validate($user, null, ['Create']))) {
            return $response;
        }

        return new JsonResponse(null, 204);
    }

    /**
     * Retrieve users list.
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param PaginationManager $pm
     * @Route(name="list", methods="GET")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request, string $version, string $protocol, PaginationManager $pm): \Symfony\Component\HttpFoundation\Response
    {
        try {
            /** @var \Oka\PaginationBundle\Pagination\Page $page */
            $page = $pm->paginate('user', $request, /*['createdAt' => 'DESC']*/);
        } catch (\Oka\PaginationBundle\Exception\PaginationException $e){
            throw new BadRequestHttpException($e->getMessage(), $e);
        }

        return $this->json(
            $page->toArray(),
            $page->getPageNumber() > 1 ? 206 : 200,
            [], [ 'groups' => $request->query->has('details') ? ['details'] : ['summary'] ]
        );
    }

    /**
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param EntityManagerInterface $em
     * @param array $requestContent
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route(name="create", methods="POST")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     * @RequestContent(constraints="createConstraints");
     */
    public function create(Request $request, string $version, string $protocol, EntityManagerInterface $em, array $requestContent): \Symfony\Component\HttpFoundation\Response
    {
        if (false === isset($requestContent['password'])) {
            $requestContent['password'] = PasswordGenerator::random();
        }

        $user = $this->edit(new User(), $requestContent);

        if (null !== ($response = $this->validate($user, null, ['Create']))) {
            return $response;
        }

        $em->persist($user);
        $em->flush();

        $this->get('event_dispatcher')->dispatch(
            new PropertyVerificationEvent($user, 'email', $user->getEmail(), $requestContent['redirectUrl'] ?? null)
        );

        return new JsonResponse($user, 201);
    }

    /**
     * Read single user
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route(name="read", path="/{id}", methods="GET")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function read(Request $request, string $version, string $protocol, User $user): \Symfony\Component\HttpFoundation\Response
    {
        return $this->json($user);
    }

    /**
     * Update specific user by id given
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param User $user
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response|null
     * @Route(name="update", path="/{id}", methods="PUT")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     * @RequestContent(constraints="updateConstraints")
     */
    public function update(Request $request, string $version, string $protocol, array $requestContent, User $user, EntityManagerInterface $em): \Symfony\Component\HttpFoundation\Response
    {
        $user = $this->edit($user, $requestContent);

        if (null !== ($response = $this->validate($user, null, ['Update']))) {
            return $response;
        }

        $em->flush();

        return $this->json($user);
    }

    /**
     * Remove the specific user by id given
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param User $user
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route(name="delete", path="/{id}", methods="DELETE")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function delete(Request $request, string $version, string $protocol, User $user, EntityManagerInterface $em): \Symfony\Component\HttpFoundation\Response
    {
        try {
            $em->remove($user);
            $em->flush();
        } catch (\Exception $e) {
            throw new ConflictHttpException(
                $this->get('translator')->trans('http_error.request_cannot_be_processed', ['%id%' => $user->getId()], 'errors'), $e
            );
        }

        return $this->json(null, 204);
    }

    /**
     * Change user password
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param User $user
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     * @Route(name="change_password", path="/{id}/change/password", methods="PUT")
     * @RequestContent(constraints = "changePasswordConstraints")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function changePassword(Request $request, string $version, string $protocol, array $requestContent, User $user, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder): Response
    {
        if (false === $encoder->isPasswordValid($user, $requestContent['password'])) {
            throw new UnauthorizedHttpException(
                $this->get('translator')->trans('authentication.bad_request', [], 'errors')
            );
        }

        return $this->editPassword($user, $requestContent['newPassword'], $em);
    }

    /**
     * change user status
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param User $user
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="change_status", path="/{id}/change/status")
     * @RequestContent(constraints="statusChangeConstraints")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function changeStatus(Request $request, string $version, string $protocol, array $requestContent, User $user, EntityManagerInterface $em): Response
    {
        $user = $this->editStatus($user, $requestContent);

        if (null !== ($response = $this->validate($user, null, ['ChangeStatus']))) {
            return $response;
        }

        $em->flush();

        return $this->json(null, 204);
    }

    /**
     * Promote user
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param User $user
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="promote", path="/{id}/promote")
     * @RequestContent(constraints="roleConstraints")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function promote(Request $request, string $version, string $protocol, array $requestContent, User $user, EntityManagerInterface $em): Response
    {
        foreach ($requestContent['roles'] as $role) {
            $user->addRole($role);
        }

        $em->flush();

        return $this->json(null, 204);
    }

    /**
     * Demote user
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param User $user
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="demote", path="/{id}/demote")
     * @RequestContent(constraints="roleConstraints")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function demote(Request $request, string $version, string $protocol, array $requestContent, User $user, EntityManagerInterface $em): Response
    {
       foreach ($requestContent['roles'] as $role) {
           $user->removeRole($role);
       }

       $em->flush();

       return $this->json(null, 204);
    }

    /**
     * Check the field of user
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="check_user_field", path="/fields/check/request", methods="POST")
     * @RequestContent(constraints="fieldCheckRequestConstraints")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function checkFieldRequest(Request $request, string $version, string $protocol, array $requestContent, EntityManagerInterface $em): Response
    {
        //dd($requestContent);
        /** @var \App\Entity\User $user */
        if (!$user = $em->getRepository(User::class)->findOneBy([$requestContent['field'] => $requestContent['value']])) {
            throw new NotFoundHttpException(
                $this->get('translator')->trans('http_error.not_found', ['%id%' => $requestContent['value']], 'errors')
            );
        }

        $event = new PropertyVerificationEvent($user, $requestContent['field'], $requestContent['value'], $requestContent['redirectUrl'] ?? null, $requestContent['context'] ?? []);
        $this->get('event_dispatcher')->dispatch($event);

        if (!$response = $event->getResponse()) {
            $response = new JsonResponse(null, 204);
        }

        return $response;
    }

    /**
     * Confirm field checking
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param $token
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="confirm_field_check", path="/fields/check/confirm/{token}", methods={"GET", "POST"})
     * @RequestContent(constraints="fieldCheckConfirmConstraints")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function fieldCheckConfirm(Request $request, string $version, string $protocol, array $requestContent, $token, EntityManagerInterface $em): Response
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        $user->addPropertiesVerified('email' === $requestContent['field'] ? $user::PROPERTY_VERIFIED_EMAIL : $user::PROPERTY_VERIFIED_PHONE_NUMBER);
        $user->setEnabled(true);

        $em->flush();

        if (true === isset($requestContent['redirectUrl'])) {
            return new RedirectResponse($requestContent['redirectUrl']);
        }

        return $this->json(null, 204);
    }

    /**
     * Password resetting request.
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="password_resetting_request", path="/password/resetting/request", methods="POST")
     * @RequestContent(constraints="passwordResettingResetRequestConstraints")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function passwordResettingRequest(Request $request, string $version, string $protocol, array $requestContent, EntityManagerInterface $em): Response
    {
        /** @var \App\Entity\User $user */
        if (!$user = $em->getRepository(User::class)->loadUserByUsername($requestContent['username'])) {
            throw new NotFoundHttpException(
                $this->get('translator')->trans('http_error.not_found', ['%id%' => $requestContent['username']], 'errors')
            );
        }

        if (false === isset($requestContent['channel'])) {
            $requestContent['channel'] = 'email';
        }

        /** @var \App\Event\PropertyVerificationEvent $event */
        $event = $this->get('event_dispatcher')->dispatch(new PropertyVerificationEvent(
            $user,
            'password',
            'email' === $requestContent['channel'] ? $user->getEmail() : $user->getPhoneNumber(),
            $requestContent['resetUrl'] ?? null,
            $requestContent
        ));

        if (!$response = $event->getResponse()) {
            $response = new JsonResponse(null, 204);
        }

        return $response;
    }

    /**
     * Password resetting reset
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @param $token
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="password_resetting_reset", path="/password/resetting/reset/{token}", methods={"POST", "GET"})
     * @RequestContent(constraints="passwordResettingResetConstraints")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function passwordResettingReset(Request $request, string $version, string $protocol, array $requestContent, string $token, EntityManagerInterface $em): Response
    {
        if (null === ($user = $this->getUser())) {
            $user = $em->getRepository(User::class)->loadUserByUsername($requestContent['password']['username']);
        }

        return $this->editPassword($user, $requestContent['password']['password'], $em);
    }

    private function edit (User $user, array $requestContent = []): User
    {
        /*$propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableMagicCall()
            ->getPropertyAccessor();

        foreach ($requestContent as $key => $value) {
            $propertyAccessor->setValue($user, $key, $value);
        }

        return $user;*/

        if (true === isset($requestContent['username'])) {
            $user->setUsername($requestContent['username']);
        }
        if (true === isset($requestContent['email'])) {
            $user->setEmail($requestContent['email']);
        }
        if (true === isset($requestContent['phoneNumber'])) {
            $user->setPhoneNumber($requestContent['phoneNumber']);
        }
        if (true === isset($requestContent['password'])) {
            $user->setPlainPassword($requestContent['password']);
            $user->setPasswordChanged(false);
            $user->setPassword('');
        }
        if (true === isset($requestContent['roles'])) {
            $user->setRoles($requestContent['roles']);
        }
        if (true === isset($requestContent['lastName'])) {
            $user->setLastName($requestContent['lastName']);
        }
        if (true === isset($requestContent['firstName'])) {
            $user->setFirstName($requestContent['firstName']);
        }

        $this->editStatus($user, $requestContent);

        return $user;
    }

    private function editStatus(User $user, array $requestContent): User
    {
        if (true === isset($requestContent['locked'])) {
            $user->setLocked($requestContent['locked']);
        }
        if (true === isset($requestContent['accountExpiresAt'])) {
            $user->setAccountExpiresAt(\DateTime::createFromFormat(\DateTime::ISO8601, $requestContent['accountExpiresAt']));
        }

        return $user;
    }

    private function editPassword(User $user, string $password, EntityManagerInterface $em)
    {
        $user->setPlainPassword($password);

        if (null !== ($response = $this->validate($user, null, ['ChangePassword']))) {
            return $response;
        }

       $user->setPasswordChanged(true)
           ->setEnabled(true)
           ->setPassword('');
        $em->flush();

        return new JsonResponse(null, 204);
    }

    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            'event_dispatcher' => '?'.EventDispatcherInterface::class,
        ]);
    }

    private static function itemsConstraints(bool $required): Assert\Collection
    {
        $ClassName = true === $required ? Assert\Required::class : Assert\Optional::class;
        $statusChangeConstraints = self::statusChangeConstraints();

        return new Assert\Collection([
            'email' => new $ClassName(new Assert\Email()),
            'username' => new $ClassName(new Assert\NotBlank()),
            'password' => new Assert\Optional(new Assert\NotBlank()),
            'roles' => new Assert\Optional(new Assert\All(new Assert\NotBlank())),
            'firstName' => new $ClassName(new Assert\NotBlank()),
            'lastName' => new Assert\Optional(new Assert\NotBlank()),
            'phoneNumber' => new $ClassName(new Assert\Regex([
                'pattern' => '#^[0-9]{10,}$#',
                'message' => 'user.phone_number.invalid'
            ])),
            'lastLogin' => new Assert\Optional([new Assert\NotBlank(), new Assert\DateTime(['format' => \DateTime::ISO8601])]),
            'locked' => $statusChangeConstraints->fields['locked'],
            'accountExpiresAt' => $statusChangeConstraints->fields['accountExpiresAt']
        ]);
    }

    private static function statusChangeConstraints(): Assert\Collection
    {
        return new Assert\Collection([
            'locked' => new Assert\Optional(new Assert\Type(['type' => 'boolean'])),
            'accountExpiresAt' => new Assert\Optional([new Assert\NotBlank(), new Assert\DateTime(['format' => \DateTime::ISO8601])])
        ]);
    }

    private static function changePasswordConstraints(): Assert\Collection
    {
        return new Assert\Collection([
            'password' => new Assert\Required(new Assert\NotBlank()),
            'newPassword' => new Assert\Required(new Assert\NotBlank())
        ]);
    }

    private static function checkConstraints(): Assert\Collection
    {
        return self::itemsConstraints(false);
    }

    private static function createConstraints(): Assert\Collection
    {
        $constraints = self::itemsConstraints(true);
        $constraints->fields['redirectUrl'] = new Assert\Required(new Assert\Url());

        return $constraints;
    }

    private static function updateConstraints(): Assert\Collection
    {
        $constraints = self::itemsConstraints(false);
        unset($constraints->fields['password']);

        return $constraints;
    }

    private static function roleConstraints(): Assert\Collection
    {
        return new Assert\Collection([
           'roles' => new Assert\Optional(new Assert\All(new Assert\NotBlank()))
        ]);
    }

    private static function fieldCheckRequestConstraints(): Assert\Collection
    {
        $constraints = self::fieldCheckConfirmConstraints();
        $constraints->fields['value'] = new Assert\Required(new Assert\NotBlank());
        $constraints->fields['context'] = new Assert\Optional(new Assert\Collection([
            'platform' => new Assert\Optional(new Assert\Choice(['choices' => ['default', 'ios', 'android']])),
            'hash' => new Assert\Optional(new Assert\NotBlank())
        ]));

        return $constraints;
    }

    private static function fieldCheckConfirmConstraints(): Assert\Collection
    {
        return new Assert\Collection([
            'field' => new Assert\Required(new Assert\Choice(['choices' => ['email', 'phoneNumber']])),
            'redirectUrl' => new Assert\Required(new Assert\Url())
        ]);
    }

    private static function passwordResettingResetRequestConstraints(): Assert\Collection
    {
        return new Assert\Collection([
            'username' => new Assert\Required(new Assert\NotBlank()),
            'resetUrl' => new Assert\Optional(new Assert\Url()),
            'channel' => new Assert\Optional(new Assert\Choice(['choices' => ['email', 'sms']]))
        ]);
    }

    private static function passwordResettingResetConstraints(): Assert\Collection
    {
        return new Assert\Collection([
            'password' => new Assert\Required(new Assert\NotBlank())
        ]);
    }
}
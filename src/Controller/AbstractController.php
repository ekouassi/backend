<?php


namespace App\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Oka\PaginationBundle\Pagination\PaginationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as BaseAbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class AbstractController
 * @package App\Controller
 */
class AbstractController extends BaseAbstractController
{
    public static function getSubscribedServices()
    {
        return array_merge(
           parent::getSubscribedServices(),
           [
               'validator' => '?'.ValidatorInterface::class,
               'translator' => '?'. TranslatorInterface::class,
               'doctrine.orm.entity_manager' => '?'. EntityManagerInterface::class,
               'oka_pagination.manager' => '?'. PaginationManager::class
           ]
        );
    }

    protected function json($data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        /*$context = array_merge([
            AbstractObjectNormalizer::GROUPS => ['details'],
            AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
            AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true
                ], $context);

        return $this->json($data, $status, $headers, $context);*/
        return parent::json($data, $status, $headers, $context ?? ['groups' => ['details']]);
    }

    protected function validate($data, Constraint $constraints = null, array $groups = []): ?Response
    {
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = $this->get('validator')->validate($data, $constraints, $groups);

        return $errors->count() > 0
            ? $this->json($errors, 400, ['title' => $this->get('translator')->trans('http_error.bad_request', [], 'errors')])
            : null;
    }
}
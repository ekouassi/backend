<?php


namespace App\Controller;

use Jose\Component\Core\JWKSet;
use Jose\Component\KeyManagement\JWKFactory;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class JWKSetController
 * @package App\Controller
 */
class JWKSetController extends AbstractController
{
    public function __invoke()
    {
        $jwtSet = new JWKSet([
            JWKFactory::createFromKeyFile((string)[
                $this->getParameter('jwt_public_key'),
                $this->getParameter('jwt_passphrase'),
                [
                    'alg' => 'RS256',
                    'use' => 'sig'
                ]
            ])
        ]);

        return new Response(json_encode($jwtSet), Response::HTTP_OK, ['Content-Type' => 'application/jwk-set+json; charset=UTF-8']);
    }
}
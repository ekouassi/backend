<?php


namespace App\Controller;

use Oka\InputHandlerBundle\Annotation\AccessControl;
use Oka\InputHandlerBundle\Annotation\RequestContent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class ProfileController
 * @package App\Controller
 * @Route(name="app_me", path="/me")
 */
class ProfileController extends AbstractController
{
    /**
     * Read user profile
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @return Response
     * @Route(name="read", methods="GET")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function read(Request $request, string $version, string $protocol): Response
    {
        return $this->json($this->getUser());
    }

    /**
     * Update user profile
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param array $requestContent
     * @return Response
     * @Route(name="update", methods={"PUT", "PATCH"})
     * @AccessControl(version="v1", protocol="rest", formats="json")
     * @RequestContent(constraints="updateConstraints")
     */
    public function update(Request $request, string $version, string $protocol, array $requestContent): Response
    {
        return $this->forward('App\Controller\UserController::update', [
           'version' => $version,
           'protocol' => $protocol,
           'requestContent' => $requestContent,
           'user' => $this->getUser()
        ]);
    }

    /**
     * Delete user profile
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @return Response
     * @Route(name="delete", methods="DELETE")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function delete(Request $request, string $version, string $protocol): Response
    {
        return $this->forward('App\Controller\UserController::delete', [
           'version' => $version,
           'protocol' => $protocol,
           'user' => $this->getUser()
        ]);
    }

    private static function updateConstraints(): Assert\Collection
    {
        return new Assert\Collection([
            'email' => new Assert\Optional(new Assert\Email()),
            'phoneNumber' => new Assert\Optional(new Assert\Regex([
                'pattern' => '#^[0-9]{10,}$#',
                'message' => 'user.phone_number.invalid',
            ])),
            'lastName' => new Assert\Optional(new Assert\NotBlank()),
            'firstName' => new Assert\Optional(new Assert\NotBlank()),
        ]);
    }
}
<?php


namespace App\Controller;

use App\Entity\RefreshToken;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Oka\InputHandlerBundle\Annotation\AccessControl;
use Oka\PaginationBundle\Pagination\PaginationManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class AuthenticationController
 * @package App\Controller
 * @Route(name="app_authentication_tokens", path="/auth/tokens", requirements={"id": "^[[:alnum:]_-]+$"})
 */
class AuthenticationController extends AbstractController
{
    /**
     * Validate JWT's access Token
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param TokenStorageInterface $tokenStorage
     * @Route(name="validate", methods={"GET", "HEAD"})
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function validateToken(Request $request, string $version, string $protocol, TokenStorageInterface $tokenStorage): Response
    {
        /** @var \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token */
        $token = $tokenStorage->getToken();

        if ($request->isMethod('HEAD')) {
            return new JsonResponse(null, 204);
        } else {
            /** @var \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token */
            $token = $tokenStorage->getToken();
            $data = [
                'user' => $token->getUser()
            ];

            if ($token instanceof JWTUserToken) {
                $payload = $token->getAttribute('payload');
                $data['tokens'] = [
                    'accessToken' => [
                        'id' => $token->getCredentials(),
                        'issuedAt' => date('c', $payload['iat']),
                        'expiresAt' => date('c', $payload['exp'])
                    ]
                ];
            }

            return $this->json($data);
        }
    }

    /**
     * Retrieves the refresh tokens
     *
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param PaginationManager $pm
     * @return Response
     * @Route(name="list_refresh_tokens", path="/refresh", methods="GET")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function listRefreshTokens(Request $request, string $version, string $protocol, PaginationManager $pm): Response
    {
        $criteria = [];
        $user = $this->getUser();

        if ($user instanceof User) {
            $criteria = ['userId' => $user->getId()];
        }

        try {
            /** @var \Oka\PaginationBundle\Pagination\Page $page */
            $page = $pm->paginate('refresh_token', $request, $criteria);
        } catch (\Oka\PaginationBundle\PaginationException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        }

        return $this->json(
            $page->toArray(),
            $page->getPageNumber() > 1 ? 206 : 200,
            [],
            ['groups' => $request->query->has('details') ? ['details'] : ['summary']]
        );
    }

    /**
     * @param Request $request
     * @param string $version
     * @param string $protocol
     * @param $id
     * @param EntityManagerInterface $em
     * @return Response
     * @Route(name="revoke_refresh", path="/refresh/{id}", methods="DELETE")
     * @AccessControl(version="v1", protocol="rest", formats="json")
     */
    public function revokeRefreshToken(Request $request, string $version, string $protocol, $id, EntityManagerInterface $em): Response
    {
        $em->getRepository(RefreshToken::class)->deleteBy(['id' => $id, 'user' => $this->getUser()]);

        return $this->json(null, 204);
    }
}
<?php

namespace App\Test;

use App\Entity\OtpToken;
use App\Entity\RefreshToken;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 */
class WebTestCase extends BaseWebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected \Symfony\Bundle\FrameworkBundle\KernelBrowser $client;

    public static function setUpBeforeClass(): void
    {
        static::bootKernel();

        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = static::$container->get('doctrine.orm.entity_manager');

        $em->getRepository(RefreshToken::class)->deletedBy();
        $em->getRepository(OtpToken::class)->deletedBy();
        $em->getRepository(User::class)->deletedBy();
    }

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }
}
<?php


namespace App\Repository;

use Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Doctrine\ORM\Utility\PersisterHelper;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class EntityRepository
 * @package App\Repository
 */
class EntityRepository extends BaseEntityRepository
{
    public function deleteBy(array $criteria = []): void
    {
        $builder = $this->createQueryBuilder( 'r')
            ->delete($this->_entityName, 'r');

        foreach ($criteria as $field => $value) {
            $types = PersisterHelper::getTypeOfField($field, $this->_class, $this->_em);
            $builder->andWhere(sprintf('%1$s.%2$s = :%2$s', 'r', $field));
            $builder->setParameter($field, $value, $types[0]);
        }

        $builder->getQuery()
            ->execute();
    }
}
<?php


namespace App\Repository;

use Doctrine\ORM\Query\Expr;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class TokenRepository
 * @package App\Repository
 */
class TokenRepository extends EntityRepository
{
    public function validate(string $id, UserInterface $user): bool
    {
        return (bool) $this->createQueryBuilder('t')
            ->select((new Expr())->countDistinct('t.id'))
            ->where('t.id = :id AND t.user = :user AND (t.expiresAt IS NULL OR t.expiresAt > CURRENT_TIMESTAMP())')
            ->setParameters(['id' => $id, 'user' => $user])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function garbage(): void
    {
        $this->createQueryBuilder('t')
            ->delete()
            ->where('t.expiresAt IS NOT NULL AND t.expiresAt < CURRENT_TIMESTAMP()')
            ->getQuery()
            ->execute();
    }
}
<?php


namespace App\Repository;


use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends EntityRepository implements UserLoaderInterface
{
    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface::loadUserByUsername()
     */
    public function loadUserByUsername($username)
    {
       return $this->createQueryBuilder('u')
            ->where('u.email = :username OR u.username = :username OR u.phoneNumber = :username')
            ->setParameter('username', $username)
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
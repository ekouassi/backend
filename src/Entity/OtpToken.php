<?php

namespace App\Entity;

use App\Behavior\Timestampable;
use App\Model\AbstractToken;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="opt_token")
 */
class OtpToken extends AbstractToken
{
    use Timestampable;

    /**
     * @ORM\Column(name="emitter_channel", type="string", columnDefinition="enum('email', 'sms')", nullable=true)
     * @Serializer\SerializedName("emitterChannel")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var string
     */
    protected string $emitterChannel;

    /**
     * @ORM\Column(name="receiver_address", type="string")
     * @Serializer\SerializedName("receiverAddress")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var string
     */
    protected string $receiverAddress;

    /**
     * @ORM\Column(name="fields", type="json")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var array
     */
    protected array $fields;

    /**
     * @ORM\Column(name="issued_at", type="datetime")
     * @Serializer\SerializedName("issuedAt")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var \DateTime
     */
    protected \DateTime $issuedAt;

    /**
     * @ORM\Column(name="expires_at", type="date")
     * @Serializer\SerializedName("expiresAt")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var \DateTime
     */
    protected \DateTime $expiresAt;

    public function __construct()
    {
        $this->issuedAt = new \DateTime('now');
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return (string) $this->id;
    }

    /**
     * @return string
     */
    public function getEmitterChannel(): string
    {
        return $this->emitterChannel;
    }

    /**
     * @param string $emitterChannel
     * @return OtpToken
     */
    public function setEmitterChannel(string $emitterChannel): self
    {
        $this->emitterChannel = $emitterChannel;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiverAddress(): string
    {
        return $this->receiverAddress;
    }

    /**
     * @param string $receiverAddress
     * @return OtpToken
     */
    public function setReceiverAddress(string $receiverAddress): self
    {
        $this->receiverAddress = $receiverAddress;
        return $this;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     * @return OtpToken
     */
    public function setFields(array $fields): self
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getIssuedAt(): \DateTime
    {
        return $this->issuedAt;
    }

    /**
     * @param \DateTime $issuedAt
     * @return OtpToken
     */
    public function setIssuedAt(\DateTime $issuedAt): self
    {
        $this->issuedAt = $issuedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt(): \DateTime
    {
        return $this->expiresAt;
    }

    /**
     * @param \DateTime $expiresAt
     * @return OtpToken
     */
    public function setExpiresAt(\DateTime $expiresAt): self
    {
        $this->expiresAt = $expiresAt;
        return $this;
    }
}
<?php

namespace App\Entity;

use App\Behavior\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="username", errorPath="[username]", message="user.username.already_used", groups={"Create", "Update"})
 * @UniqueEntity(fields="email", errorPath="[email]", message="user.email.already_used", groups={"Create", "Update"})
 * @UniqueEntity(fields="phoneNumber", errorPath="[phoneNumber]", message="user.phone_number.already_used", groups={"Create", "Update"})
 */
class User implements AdvancedUserInterface
{
    use Timestampable;

    public const DEFAULT_ROLE = "ROLE_USER";
    public const ROLE_ADMIN = "ROLE_ADMIN";
    public const ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
    public const ROLE_CUSTOMER = "ROLE_CUSTOMER";
    public const ROLE_VENDOR = "ROLE_VENDOR";

    public const PROPERTY_VERIFIED_EMAIL = 1;
    public const PROPERTY_VERIFIED_PHONE_NUMBER = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid_binary_ordered_time", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidOrderedTimeGenerator")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var \Ramsey\Uuid\UuidInterface
     */
    protected \Ramsey\Uuid\UuidInterface $id;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Serializer\Groups({"summary", "details"})
     *
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Serializer\Groups({"summary", "details"})
     *
     * @var string
     */
    protected $username;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Groups({"summary", "details"})
     *
     * @var string
     */
    protected $password;

    /**
     * @Assert\NotCompromisedPassword(groups={"Check", "Create", "ChangePassword"})
     *
     * @var string
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Serializer\Groups({"summary", "details"})
     *
     * @var array
     */
    protected $roles;

    /**
     * @ORM\Column(name="first_name", type="string")
     * @Serializer\SerializedName("firstName")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var string
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", nullable=true)
     * @Serializer\SerializedName("lastName")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var string
     */
    protected $lastName;

    /**
     * @ORM\Column(name="phone_number", type="string")
     *
     * @var string
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     * @Serializer\SerializedName("lastLogin")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var \DateTime
     */
    protected $lastLogin;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     * @Serializer\Groups({"summary", "details"})
     *
     * @var bool
     */
    protected $enabled;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     * @Serializer\Groups({"summary", "details"})
     *
     * @var bool
     */
    protected $locked;

    /**
     * @ORM\Column(name="properties_verified", type="smallint", options={"unsigned": true, "default": 0})
     * @Serializer\SerializedName("propertiesVerified")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var int
     */
    protected $propertiesVerified;

    /**
     * @ORM\Column(name="password_changed", type="boolean", options={"default": 0})
     * @Serializer\SerializedName("passwordChanged")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var bool
     */
    protected $passwordChanged;

    /**
     * @ORM\Column(name="account_expires_at", type="datetime", nullable=true)
     * @Serializer\SerializedName("accountExpiresAt")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var \DateTime
     */
    protected $accountExpiresAt;

    public function __construct()
    {
        $this->roles = [self::DEFAULT_ROLE];
        $this->enabled = true;
        $this->locked = false;
        $this->propertiesVerified = 0;
        $this->passwordChanged = 0;
    }

    /**
     * @return \Ramsey\Uuid\UuidInterface
     */
    public function getId(): string
    {
        return (string) $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     * @return $this
     */
    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function addRole(string $role): self
    {
        $role = strtoupper($role);
        if ($role !== self::DEFAULT_ROLE) {
            if (!in_array($role, $this->roles, true)) {
                $this->roles[] = $role;
            }
        }

        return $this;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = [];

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    public function removeRole(string $role): self
    {
        $role = strtoupper($role);

        if ($role !== self::DEFAULT_ROLE) {
            if (false !== ($key = array_search($role, $this->roles, true))) {
                unset($this->roles[$key]);
                $this->roles = array_values($this->roles);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getFullName(): string
    {
        return $this->getFirstName() .' '. $this->getLastName();
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return User
     */
    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastLogin(): ?\DateTime
    {
        return $this->lastLogin;
    }

    /**
     * @param \DateTime $lastLogin
     * @return User
     */
    public function setLastLogin(\DateTime $lastLogin): self
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return User
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     * @return User
     */
    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;
        return $this;
    }

    /**
     * @return int
     */
    public function isPropertiesVerified(): int
    {
        return $this->propertiesVerified;
    }

    /**
     * @param int $propertiesVerified
     * @return User
     */
    public function setPropertiesVerified(int $propertiesVerified): self
    {
        $this->propertiesVerified = $propertiesVerified;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasPropertiesVerified(): bool
    {
        return (self::PROPERTY_VERIFIED_EMAIL | self::PROPERTY_VERIFIED_PHONE_NUMBER) === $this->propertiesVerified;
    }

    /**
     * @return bool
     */
    public function hasEmailVerified(): bool
    {
        return ($this->propertiesVerified & self::PROPERTY_VERIFIED_EMAIL) === $this->propertiesVerified;
    }

    public function hasPhoneNumberVerified(): bool
    {
        return ($this->propertiesVerified & self::PROPERTY_VERIFIED_PHONE_NUMBER) === $this->propertiesVerified;
    }

    public function addPropertiesVerified(int $propertiesVerified): self
    {
        $this->propertiesVerified |= $propertiesVerified;

        return $this;
    }

    public function removePropertiesVerified(int $propertiesVerified): self
    {
        $this->propertiesVerified &= ~$propertiesVerified;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPasswordChanged(): bool
    {
        return $this->passwordChanged;
    }

    /**
     * @param bool $passwordChanged
     * @return User
     */
    public function setPasswordChanged(bool $passwordChanged): self
    {
        $this->passwordChanged = $passwordChanged;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getAccountExpiresAt(): ?\DateTime
    {
        return $this->accountExpiresAt;
    }

    /**
     * @param \DateTime $accountExpiresAt
     * @return User
     */
    public function setAccountExpiresAt(\DateTime $accountExpiresAt): self
    {
        $this->accountExpiresAt = $accountExpiresAt;
        return $this;
    }

    public function isAccountNonExpired()
    {
       return $this->accountExpiresAt instanceof \DateTime ? $this->accountExpiresAt->getTimestamp() >= time() : true;
    }

    public function isAccountNonLocked()
    {
       return !$this->locked;
    }

    public function isCredentialsNonExpired()
    {}

    /** @return string|void|null */
    public function getSalt()
    {}

    public function eraseCredentials()
    {
      $this->plainPassword = null;
    }
}
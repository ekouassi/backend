<?php

namespace App\Entity;

use App\Model\AbstractToken;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="refresh_token")
 * @ORM\HasLifecycleCallbacks()
 */
class RefreshToken extends AbstractToken
{
}
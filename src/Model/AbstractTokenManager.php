<?php


namespace App\Model;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class AbstractTokenManager
 * @package App\Model
 */
abstract class AbstractTokenManager
{
    protected EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function saveToken(AbstractToken $token)
    {
        if (false === $this->em->contains($token)) {
            $this->em->persist($token);
        }

        $this->em->flush();
    }

    public function findToken(string $id): ?AbstractToken
    {
        return $this->em->find($this->getClassName(), $id);
    }

    public function validateToken(string $id, UserInterface $user): bool
    {
        return $this->em->getRepository($this->getClassName())->validate($id, $user);
    }

    public function revokeToken(AbstractToken $token, bool $andFlush = true): void
    {
        $this->em->remove($token);

        if (true === $andFlush) {
            $this->em->flush();
        }
    }

    abstract public function createToken(UserInterface $user, \DateTime $expiresAt = null, bool $andFlush = true): AbstractToken;
    abstract protected function getClassName(): string ;
}
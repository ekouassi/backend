<?php


namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation as Serializer;
/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class AbstractToken
 * @package App\Model
 * @ORM\MappedSuperclass(repositoryClass="App\Repository\TokenRepository")
 * @ORM\HasLifecycleCallbacks()
 */
abstract class AbstractToken
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="NONE")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="issued_at", type="datetime")
     * @Serializer\Groups({"summary", "details"})
     * @Serializer\SerializedName("issuedAt")
     *
     * @var \DateTime
     */
    protected $issuedAt;

    /**
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     * @Serializer\SerializedName("expiresAt")
     * @Serializer\Groups({"summary", "details"})
     *
     * @var \DateTime
     */
    protected $expiresAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\SerializedName("user")
     * @Serializer\Groups({"details"})
     *
     * @var UserInterface
     */
    protected $user;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return AbstractToken
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getIssuedAt(): \DateTime
    {
        return $this->issuedAt;
    }

    /**
     * @param \DateTime $issuedAt
     * @return AbstractToken
     */
    public function setIssuedAt(\DateTime $issuedAt): self
    {
        $this->issuedAt = $issuedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpiresAt(): ?\DateTime
    {
        return $this->expiresAt;
    }

    /**
     * @param \DateTime $expiresAt
     * @return AbstractToken
     */
    public function setExpiresAt(\DateTime $expiresAt): self
    {
        $this->expiresAt = $expiresAt;
        return $this;
    }


    public function removeExpiresAt(): self
    {
        $this->expiresAt = null;

        return $this;
    }

    public function isExpired(): bool
    {
        return $this->expiresAt instanceof \DateTime && $this->expiresAt->getTimestamp() < time();
    }

    /**
     * @return \Symfony\Component\Security\Core\UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @param \Symfony\Component\Security\Core\UserInterface $user
     * @return AbstractToken
     */
    public function setUser(UserInterface $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->issuedAt = new \DateTime();
    }

}
<?php


namespace App\Serializer;



use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class UserInterfaceNormalizer
 * @package App\Serializer
 */
class UserInterfaceNormalizer implements ContextAwareNormalizerInterface, CacheableSupportsMethodInterface
{
    private \Symfony\Component\Serializer\Normalizer\ObjectNormalizer $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @inheritDoc
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        $data['username'] = $object->getUsername();
        $data['roles'] = $object->getRoles();

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof UserInterface;
    }


    public function hasCacheableSupportsMethod(): bool
    {
        return false;
    }
}
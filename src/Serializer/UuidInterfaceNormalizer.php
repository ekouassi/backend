<?php


namespace App\Serializer;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class UuidInterfaceNormalizer
 * @package App\Serializer
 */
class UuidInterfaceNormalizer implements ContextAwareNormalizerInterface, CacheableSupportsMethodInterface
{
    private \Symfony\Component\Serializer\Normalizer\ObjectNormalizer $normalizer;

    /**
     * UuidInterfaceNormalizer constructor.
     * @param \Symfony\Component\Serializer\Normalizer\ObjectNormalizer $normalizer
     */
    public function __construct(\Symfony\Component\Serializer\Normalizer\ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }


    /**
     * @return bool
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return false;
    }

    /**
     * @param mixed $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return $object->_toString();
    }

    /**
     * @param mixed $data
     * @param string|null $format
     * @param array $context
     * @return bool
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof UuidInterface;
    }


}
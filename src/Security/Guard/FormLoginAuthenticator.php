<?php


namespace App\Security\Guard;

use App\Entity\User;
use App\Service\RefreshTokenManager;
use Doctrine\ORM\EntityManagerInterface;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\PreAuthenticationJWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Oka\InputHandlerBundle\Service\ErrorResponseFactory;
use Oka\InputHandlerBundle\Util\RequestUtil;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class FormLoginAuthenticator
 * @package App\Security\Guard
 */
class FormLoginAuthenticator extends AbstractGuardAuthenticator
{
    public const AUTH_METHOD_PASSWORD = "password";
    public const AUTH_METHOD_ACCESS_TOKEN = "accessToken";
    public const AUTH_METHOD_REFRESH_TOKEN = "refreshToken";

    private EntityManagerInterface $em;
    private RefreshTokenManager $refreshTokenManager;
    private JWTTokenManagerInterface $jwtTokenManager;
    private UserPasswordEncoderInterface $encoder;
    private ValidatorInterface $validator;
    private SerializerInterface $serializer;
    private TranslatorInterface $translator;
    private ErrorResponseFactory $errorFactory;

    /**
     * @var string
     */
    private string $currentAuthMethod;

    public function __construct(
        EntityManagerInterface $em,
        RefreshTokenManager $refreshTokenManager,
        JWTTokenManagerInterface $jwtTokenManager,
        UserPasswordEncoderInterface $encoder,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        TranslatorInterface $translator,
        ErrorResponseFactory $errorFactory
    )
    {
        $this->em = $em;
        $this->refreshTokenManager = $refreshTokenManager;
        $this->jwtTokenManager = $jwtTokenManager;
        $this->encoder = $encoder;
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->translator = $translator;
        $this->errorFactory = $errorFactory;
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::supports()
     */
    public function supports(Request $request)
    {
        return
            $request->isMethod('POST')
            && 'app_authentication_tokens_create' === $request->attributes->get('_route');
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::getCredentials()
     */
    public function getCredentials(Request $request)
    {
        $credentials = RequestUtil::getContentLikeArray($request);

        $tokenConstraints = new Assert\Collection([
            'token' => new Assert\Required(new Assert\Collection([
                'id' => new Assert\Required(new Assert\NotBlank())
            ]))
        ]);

        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = $this->validator->validate($credentials, new Assert\Collection([
            'identity' => new Assert\Collection([
                'methods' => new Assert\All(new Assert\Choice(['choices' => [
                    self::AUTH_METHOD_PASSWORD,
                    self::AUTH_METHOD_ACCESS_TOKEN,
                    self::AUTH_METHOD_REFRESH_TOKEN
                ]])),
                self::AUTH_METHOD_PASSWORD => new Assert\Optional(new Assert\Collection([
                    'user' => new Assert\Required(new Assert\Collection([
                        'username' => new Assert\Required(new Assert\NotBlank()),
                        'password' => new Assert\Required(new Assert\NotBlank())
                    ]))])),
                self::AUTH_METHOD_ACCESS_TOKEN => new Assert\Optional($tokenConstraints),
                self::AUTH_METHOD_REFRESH_TOKEN => new Assert\Optional($tokenConstraints)
            ])
        ]));

        if (0 < $errors->count()) {
            throw new CustomUserMessageAuthenticationException(
                $this->translator->trans('http_error.bad_request', [], 'errors')
            );
        }

        return $credentials;
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::getUser()
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $lastError = null;
        $this->currentAuthMethod = null;

        foreach ($credentials['identity']['methods'] as $value) {
            if (null === ($method = $credentials['identity'][$value] ?? null)) {
                throw new AuthenticationCredentialsNotFoundException();
            }
            $this->currentAuthMethod = $value;

            try {
                switch (true) {
                    case self::AUTH_METHOD_PASSWORD === $value:
                        $identity = $method['user'] ?? [];

                        if (true === isset($identity['username'])) {

                            try {
                                /** @var \Symfony\Component\Security\Core\User\UserInterface $user */
                                return $userProvider->loadUserByUsername($identity['username']);
                            } catch (UsernameNotFoundException $e) {
                                $lastError = null;
                            }
                        }
                        break;
                    case self::AUTH_METHOD_REFRESH_TOKEN === $value:
                        $refreshToken = $method['token'] ?? [];

                        if (true === isset($refreshToken['id']) && $refreshToken = $this->refreshTokenManager->findToken($refreshToken['id'])) {
                            if (true === $refreshToken->isExpired()) {
                                throw new CredentialsExpiredException();
                            }

                            return $refreshToken->getUser();
                        }
                        break;
                    case self::AUTH_METHOD_ACCESS_TOKEN === $value:
                        $refreshToken = $method['token'] ?? [];

                        if (true === isset($refreshToken['id'])) {
                            try {
                                $payload = $this->jwtTokenManager->decode(new PreAuthenticationJWTUserToken($refreshToken['id']));
                            } catch (JWTDecodeFailureException $e) {
                                if (JWTDecodeFailureException::EXPIRED_TOKEN === $e->getReason()) {
                                    throw new CredentialsExpiredException();
                                }
                            }

                            if (true === isset($payload) && false !== $payload) {
                                $idClaim = $this->jwtTokenManager->getUserIdClaim();

                                if (true === isset($payload[$idClaim])) {
                                    try {
                                        /** @var \Symfony\Component\Security\Core\User\UserInterface $user */
                                        return $userProvider->loadUserByUsername($idClaim);
                                    } catch (UsernameNotFoundException $e) {
                                        $lastError = null;
                                    }
                                }
                            }
                        }
                        break;
                    default:
                        continue 2;
                }
            } catch (\Throwable $e) {
                $lastError = $e;
            }
        }
        throw $lastError ?? new BadCredentialsException();
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::checkCredentials()
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if (self::AUTH_METHOD_ACCESS_TOKEN === $this->currentAuthMethod || self::AUTH_METHOD_REFRESH_TOKEN === $this->currentAuthMethod) {
            return true;
        }

        $identity = $credentials['identity'][$this->currentAuthMethod]['user'] ?? [];
        $password = (string) $identity['password'];

        if ('' === $password) {
            throw new BadCredentialsException('The presented password must not be empty.');
        }

        return $this->encoder->isPasswordValid($user, $password);
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::onAuthenticationSuccess()
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        /** @var \Symfony\Component\Security\Core\User\UserInterface $user */
        $user = $token->getUser();
        /** @var \Lcobucci\JWT\Token $accessToken */
        $accessToken = (new Parser(new JoseEncoder()))->parse($this->jwtTokenManager->createFromPayload($user, ['iat' => time()]));
        /** @var \Lcobucci\JWT\Token\DataSet $dataSet */
        $dataSet = $accessToken->headers();
        $data = [
            'method' => $this->currentAuthMethod,
            'tokens' => [
                'accessToken' => [
                    'id' => $accessToken->toString(),
                    'issuedAt' => date('c', $dataSet->get('iat', time())),
                    'expiresAt' => date('c', $dataSet->get('exp', time() + 3600))
                ]
            ]
        ];

        if ($user instanceof User) {
            $refreshToken = $this->refreshTokenManager->createToken($user, null, false);
           // dd($refreshToken);
            $this->em->flush();
            $data['tokens']['refreshToken'] = [
                'id' => $refreshToken->getId(),
                'issuedAt' => $refreshToken->getIssuedAt()->format('c'),
                'expiresAt' => $refreshToken->getExpiresAt() instanceof \DateTime ? $refreshToken->getExpiresAt()->format('c') : null
            ];
            $data['user'] = $user;
        }

        return new JsonResponse(
          $this->serializer->serialize(
              $data,
              'json',
              [
                  AbstractObjectNormalizer::GROUPS => ['details'],
                  AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
                  AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true
              ]
          ),
            201,
            [],
            true
        );
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::onAuthenticationFailure()
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        switch (true) {
            case $exception instanceof CustomUserMessageAuthenticationException:
                $statusCode = 400;
                break;
            case $exception instanceof AuthenticationServiceException:
                $statusCode = 500;
                break;
            default:
                $statusCode = 401;
        }

        $request->setRequestFormat('json');
        $flattenException = FlattenException::createFromThrowable($exception, $statusCode, ['WWW-Authenticate' => 'Bearer']);
        $flattenException->setMessage($this->translator->trans($exception->getMessageKey(), $exception->getMessageData(), 'security'));

        return $this->errorFactory->create($flattenException, $statusCode);
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::start()
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $request->setRequestFormat('json');
        $authException = $authException ?? new BadCredentialsException();
        $flattenException = FlattenException::createFromThrowable($authException, 401, ['WWW-Authenticate' => 'Bearer']);
        $flattenException->setMessage($this->translator->trans($authException->getMessageKey(), $authException->getMessageData(), 'security'));

        return $this->errorFactory->create($flattenException, 401);
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::supportsRememberMe()
     */
    public function supportsRememberMe()
    {
        return false;
    }
}
<?php


namespace App\Security\Guard;


use App\Service\PasswordTokenManager;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Oka\InputHandlerBundle\Service\ErrorResponseFactory;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class PasswordTokenAuthenticator
 * @package App\Security\Guard
 */
class PasswordTokenAuthenticator extends AbstractGuardAuthenticator
{
    private PasswordTokenManager $passwordTokenManager;
    private TranslatorInterface $translator;
    private ErrorResponseFactory $errorFactory;

    public function __construct(PasswordTokenManager $passwordTokenManager, TranslatorInterface $translator, ErrorResponseFactory $errorFactory)
    {
        $this->passwordTokenManager = $passwordTokenManager;
        $this->translator = $translator;
        $this->errorFactory = $errorFactory;
    }

    public function supports(Request $request)
    {
       return $request->attributes->has('token') ||
           (
               $request->attributes->has('Authorization') &&
               ((bool) preg_match('#^otp [0-9]{6}$#i', $request->headers->get('Authorization')))
           );
    }

    public function getCredentials(Request $request)
    {
        if ($request->attributes->has('token')) {
            return $request->attributes->get('token');
        }

        $match = [];
        preg_match('#^otp ([0-9]{6})$#i', $request->headers->get('Authorization'), $match);

        return $match[1] ?? '';
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!$credentials) {
            throw new BadCredentialsException();
        }

        /** @var \App\Entity\OtpToken $token */
        if (!$token = $this->passwordTokenManager->findToken($credentials)) {
            throw new BadCredentialsException();
        }

        $this->passwordTokenManager->revokeToken($token);

        if (true === $token->isExpired()) {
            throw new ExpiredTokenException();
        }

        return $token->getUser();
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
       return true;
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::onAuthenticationSuccess()
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::onAuthenticationFailure()
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->setRequestFormat('json');
        $flattenException = FlattenException::createFromThrowable($exception, 401, ['WWW-Authenticate' => 'Bearer']);
        $flattenException->setMessage($this->translator->trans($exception->getMessageKey(), $exception->getMessageData(), 'security'));

       return $this->errorFactory->create($flattenException, 401);
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::start()
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $request->setRequestFormat('json');
        $authException = $authException ?? new BadCredentialsException();
        $flattenException = FlattenException::createFromThrowable($authException, 401, ['WWW-Authenticate' => 'Bearer']);
        $flattenException->setMessage($this->translator->trans($authException->getMessageKey(), $authException->getMessageData(), 'security'));

        return $this->errorFactory->create($flattenException, 401);
    }

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::supportsRememberMe()
     */
    public function supportsRememberMe()
    {
        return false;
    }
}
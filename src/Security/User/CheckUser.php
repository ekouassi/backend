<?php


namespace App\Security\User;


use App\Entity\User;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class CheckUser
 * @package App\Security\User
 */
class CheckUser implements UserCheckerInterface
{

    /**
     * {@inheritDoc}
     *
     * @param UserInterface $user
     * @return mixed
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return ;
        }

        if (false === $user->isEnabled()) {
            $e = new DisabledException('User is disabled.');
            $e->setUser($user);
            throw $e;
        }

        if (false === $user->isAccountNonLocked()) {
            $e = new LockedException('User  is locked.');
            $e->setUser($user);
            throw $e;
        }

        if (false === $user->isAccountNonExpired()) {
            $e = new AccountExpiredException('User\'s account is expired.');
            $e->setUser($user);
            throw $e;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param UserInterface $user
     * @return mixed
     */
    public function checkPostAuth(UserInterface $user)
    {
        if ($user instanceof User) {
            if (false === $user->isCredentialsNonExpired()) {
                $e = new CredentialsExpiredException('User\'s credentials have expired.');
                $e->setUser($user);
                throw $e;
            }
        }
    }
}
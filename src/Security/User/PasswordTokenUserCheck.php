<?php


namespace App\Security\User;


use App\Entity\User;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Ernest KOUASSI <ernestkouassi02@gmail.com>
 *
 * Class PasswordTokenUserCheck
 * @package App\Security\User
 */
class PasswordTokenUserCheck extends UserChecker
{
    /**
     * {@inheritDoc}
     * @see \Symfony\Component\Security\Core\User\UserChecker::checkPreAuth()
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }

        if (!$user->isLocked()) {
            $e = new LockedException('User is locked.');
            $e->setUser($user);
            throw $e;
        }

        if (!$user->isAccountNonExpired()) {
            $e = new AccountExpiredException('User account is expired.');
            $e->setUser($user);
            throw $e;
        }

    }
}